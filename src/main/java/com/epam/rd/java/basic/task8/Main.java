package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.flowers.Flower;
import com.epam.rd.java.basic.task8.flowers.Flowers;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {

		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);


//		String xmlFileName = "input.xml"; // to remove before demo


		/* --------------- DOM ---------------- */

		// get Controller
		String outputXmlFile = "output.dom.xml";
		DOMController domController = new DOMController(xmlFileName);

		if (!domController.validateXMLSchema("input.xsd", xmlFileName)){
			return;
		}

		{
			Flowers flowers = domController.getFlowers();
			flowers.getFlowers().sort(domController.getComparator());

			System.out.println(
					domController.writeInXML(flowers, outputXmlFile));

			System.out.println(flowers.getFlowers());
		}


		/* --------------- SAX ---------------- */
		
		// get Controller
		// PLACE YOUR CODE HERE
		outputXmlFile = "output.sax.xml";
		{
			SAXController saxController = new SAXController(xmlFileName);

			Flowers flowers = saxController.getFlowers();
			flowers.getFlowers().sort(saxController.getComparator());

			System.out.println(
					domController.writeInXML(flowers, outputXmlFile));

			System.out.println(flowers.getFlowers());
		}


		/* --------------- StAX ---------------- */

		// get
		outputXmlFile = "output.stax.xml";
		STAXController staxController = new STAXController(xmlFileName);
		{
			SAXController saxController = new SAXController(xmlFileName);

			Flowers flowers = saxController.getFlowers();
			flowers.getFlowers().sort(staxController.getComparator());

			System.out.println(
					domController.writeInXML(flowers, outputXmlFile));

			System.out.println(flowers.getFlowers());
		}
	}

}
