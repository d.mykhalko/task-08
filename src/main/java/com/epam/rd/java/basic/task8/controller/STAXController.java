package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flowers.Flower;
import com.epam.rd.java.basic.task8.flowers.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Comparator<Flower> comparator = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getOrigin().compareTo(o2.getOrigin());
		}
	};

	public Comparator<Flower> getComparator() {
		return comparator;
	}

	/*
	private static void getFlowers(String fileName)
			throws FileNotFoundException, XMLStreamException {

		Path path = Paths.get(fileName);

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(
				new FileInputStream(path.toFile()));

		// event iterator
		while (reader.hasNext()) {

			XMLEvent event = reader.nextEvent();

			if (event.isStartElement()) {

				StartElement element = event.asStartElement();

				switch (element.getName().getLocalPart()) {
//					case "flowers":
//						// id='1001'
//						Attribute id = element.getAttributeByName(new QName("id"));
//						System.out.printf("Staff id : %s%n", id.getValue());
//						break;
					case "flower":
						Attribute id = element.getAttributeByName(new QName("id"));
						System.out.printf("Staff id : %s%n", id.getValue());
						break;
					case "name":
						// throws StartElementEvent cannot be cast to class javax.xml.stream.events.Characters
						// element.asCharacters().getData()

						// this is still '<name>' tag, need move to next event for the character data
						event = reader.nextEvent();
						if (event.isCharacters()) {
							System.out.printf("Name : %s%n", event.asCharacters().getData());
						}
						break;
					case "role":
						event = reader.nextEvent();
						if (event.isCharacters()) {
							System.out.printf("Role : %s%n", event.asCharacters().getData());
						}
						break;
					case "salary":
						// currency='USD'
						Attribute currency = element.getAttributeByName(new QName("currency"));
						event = reader.nextEvent();
						if (event.isCharacters()) {
							String salary = event.asCharacters().getData();
							System.out.printf("Salary [Currency] : %,.2f [%s]%n",
									Float.parseFloat(salary), currency);
						}
						break;
					case "bio":
						event = reader.nextEvent();
						if (event.isCharacters()) {
							// CDATA, no problem.
							System.out.printf("Bio : %s%n", event.asCharacters().getData());
						}
						break;
				}
			}

			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				// if </staff>
				if (endElement.getName().getLocalPart().equals("staff")) {
					System.out.printf("%n%s%n%n", "---");
				}
			}

		}

	}

	 */
}