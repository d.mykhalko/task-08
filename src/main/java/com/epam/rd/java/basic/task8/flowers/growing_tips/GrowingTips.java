package com.epam.rd.java.basic.task8.flowers.growing_tips;

public class GrowingTips {
    private Temperature temperature;
    private String lighting;
    private Watering watering;

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer value, String measure) {
        temperature = new Temperature(value, measure);
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lightRequiring) {
        lighting = lightRequiring;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Integer value, String measure) {
        watering = new Watering(value, measure);
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
