package com.epam.rd.java.basic.task8.flowers.visual_parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private List<AveLenFlower> aveLenFlowers;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public List<AveLenFlower> getAveLenFlowers() {
        return aveLenFlowers;
    }

    public void setAveLenFlowers() {
        if (Objects.isNull(aveLenFlowers))
            this.aveLenFlowers = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlowers=" + aveLenFlowers +
                '}';
    }
}
