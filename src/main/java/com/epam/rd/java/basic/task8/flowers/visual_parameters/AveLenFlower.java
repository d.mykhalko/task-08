package com.epam.rd.java.basic.task8.flowers.visual_parameters;

public class AveLenFlower {
    private Integer value;
    private String measure;

    public AveLenFlower(Integer value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }
}
