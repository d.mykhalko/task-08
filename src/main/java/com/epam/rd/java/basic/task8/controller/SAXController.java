package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flowers.Flower;
import com.epam.rd.java.basic.task8.flowers.Flowers;
import com.epam.rd.java.basic.task8.flowers.growing_tips.GrowingTips;
import com.epam.rd.java.basic.task8.flowers.growing_tips.Temperature;
import com.epam.rd.java.basic.task8.flowers.growing_tips.Watering;
import com.epam.rd.java.basic.task8.flowers.visual_parameters.AveLenFlower;
import com.epam.rd.java.basic.task8.flowers.visual_parameters.VisualParameters;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Comparator<Flower> comparator = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getGrowingTips().getTemperature().getValue()
					.compareTo(o2.getGrowingTips().getTemperature().getValue());
		}
	};

	public Comparator<Flower> getComparator() {
		return comparator;
	}

	public boolean validateXMLSchema(String xsdPath, String xmlPath) {
		return true;
	}





	private StringBuilder currentValue = new StringBuilder();
	private Flowers result;
	private Flower currentFlower;

	public Flowers getResult() {
		return result;
	}

	@Override
	public void startDocument() {
		result = new Flowers(null, new ArrayList<>());
	}

	@Override
	public void startElement(
			String uri,
			String localName,
			String qName,
			Attributes attributes) {

		// reset the tag value
		currentValue.setLength(0);

		if (qName.equalsIgnoreCase("flowers")) {
			result.setConfiguration(new LinkedHashMap<>());

			for (int i = 0; i < attributes.getLength(); i++) {
				result.getConfiguration().put(attributes.getLocalName(i), attributes.getValue(i));
			}
		}

		// start of loop
		if (qName.equalsIgnoreCase("flower")) {
			currentFlower = new Flower();
		}

		if (qName.equalsIgnoreCase("visualParameters")) {
			currentFlower.setVisualParameters(new VisualParameters());
		}

		if (qName.equalsIgnoreCase("aveLenFlower")) {
			String measure = attributes.getValue("measure");
			currentFlower.getVisualParameters().setAveLenFlowers();

			currentFlower.getVisualParameters().getAveLenFlowers().add(
					new AveLenFlower(null, measure)
			);
		}

		if (qName.equalsIgnoreCase("growingTips")) {
			currentFlower.setGrowingTips(new GrowingTips());
		}

		if (qName.equalsIgnoreCase("tempreture")) {
			String measure = attributes.getValue("measure");

			currentFlower.getGrowingTips().setTemperature(
					null, measure
			);
		}

		if (qName.equalsIgnoreCase("lighting")) {
			String lightRequiring = attributes.getValue("lightRequiring");

			currentFlower.getGrowingTips().setLighting(lightRequiring);
		}

		if (qName.equalsIgnoreCase("watering")) {
			String measure = attributes.getValue("measure");

			currentFlower.getGrowingTips().setWatering(null, measure);
		}
	}

	@Override
	public void endElement(String uri,
						   String localName,
						   String qName) {

		if (qName.equalsIgnoreCase("name")) {
			currentFlower.setName(currentValue.toString());
		}

		if (qName.equalsIgnoreCase("soil")) {
			currentFlower.setSoil(currentValue.toString());
		}

		if (qName.equalsIgnoreCase("origin")) {
			currentFlower.setOrigin(currentValue.toString());
		}

		if (qName.equalsIgnoreCase("multiplying")) {
			currentFlower.setMultiplying(currentValue.toString());
		}

		// visual parameters
		if (qName.equalsIgnoreCase("stemColour")) {
			currentFlower.getVisualParameters().setStemColour(currentValue.toString());
		}

		if (qName.equalsIgnoreCase("leafColour")) {
			currentFlower.getVisualParameters().setLeafColour(currentValue.toString());
		}

		if (qName.equalsIgnoreCase("aveLenFlower")) {
			List<AveLenFlower> list = currentFlower.getVisualParameters().getAveLenFlowers();
			list.get(list.size()-1)
					.setValue(Integer.parseInt(currentValue.toString()));
		}

		if (qName.equalsIgnoreCase("tempreture")) {
			Temperature temp = currentFlower.getGrowingTips().getTemperature();
			temp.setValue(Integer.parseInt(currentValue.toString()));
		}

		if (qName.equalsIgnoreCase("watering")) {
			Watering watering = currentFlower.getGrowingTips().getWatering();
			watering.setValue(Integer.parseInt(currentValue.toString()));
		}


		// end of loop
		if (qName.equalsIgnoreCase("flower")) {
			result.getFlowers().add(currentFlower);
		}

	}

	public void characters(char[] ch, int start, int length) {
		currentValue.append(ch, start, length);
	}


	public Flowers getFlowers() throws IOException, SAXException, ParserConfigurationException {

		SAXParserFactory factory = SAXParserFactory.newInstance();

		try (InputStream is = new FileInputStream(xmlFileName)) {

			SAXParser saxParser = factory.newSAXParser();

			// parse XML and map to object, it works, but not recommend, try JAXB
			SAXController handler = new SAXController(xmlFileName);
			saxParser.parse(is, handler);

			return handler.getResult();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		SAXController controller = new SAXController("input.xml");

		Flowers res = controller.getFlowers();
		System.out.println(res.getConfiguration().entrySet());

	}

}
