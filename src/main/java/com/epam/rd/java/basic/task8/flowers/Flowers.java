package com.epam.rd.java.basic.task8.flowers;

import com.epam.rd.java.basic.task8.flowers.Flower;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.NamedNodeMap;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Flowers {

    /*
        xmlns="http://www.nure.ua"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.nure.ua input.xsd "
    * */

    private LinkedHashMap<String, String>  configuration;
    private List<Flower> flowers = new ArrayList<>();

    public Flowers(LinkedHashMap<String, String> configuration, List<Flower> flowers) {
        this.configuration = configuration;
        this.flowers = flowers;
    }

    public LinkedHashMap<String, String>  getConfiguration() {
        return configuration;
    }

    public void setConfiguration(LinkedHashMap<String, String>  configuration) {
        this.configuration = configuration;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "configuration=" + configuration +
                ", flowers=" + flowers +
                '}';
    }
}
