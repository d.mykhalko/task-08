package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.flowers.Flower;
import com.epam.rd.java.basic.task8.flowers.Flowers;
import com.epam.rd.java.basic.task8.flowers.growing_tips.GrowingTips;
import com.epam.rd.java.basic.task8.flowers.growing_tips.Temperature;
import com.epam.rd.java.basic.task8.flowers.growing_tips.Watering;
import com.epam.rd.java.basic.task8.flowers.visual_parameters.AveLenFlower;
import com.epam.rd.java.basic.task8.flowers.visual_parameters.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Comparator<Flower> comparator = Comparator.comparing(Flower::getName);

	public Comparator<Flower> getComparator() {
		return comparator;
	}

	public boolean validateXMLSchema(String xsdPath, String xmlPath) {
		if (Objects.isNull(xmlPath) || Objects.isNull(xsdPath))
			throw new IllegalArgumentException();

		try {
			SchemaFactory factory =
					SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(xsdPath));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlPath)));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	private VisualParameters getVisualParameter(Node node) {
		VisualParameters result = new VisualParameters();

		NodeList childNodes = node.getChildNodes();

		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);

			if (n.getNodeName().equals("stemColour")) {
				result.setStemColour(n.getTextContent().strip());
			} else if (n.getNodeName().equals("leafColour")) {
				result.setLeafColour(n.getTextContent().strip());
			} else if (n.getNodeName().equals("aveLenFlower")) {
				result.setAveLenFlowers();
				result.getAveLenFlowers().add(
						new AveLenFlower(Integer.parseInt(n.getTextContent().strip()),
								n.getAttributes().getNamedItem("measure").getTextContent())
				);
			}
		}
		return result;
	}

	private GrowingTips getGrowingTips(Node node) {
		GrowingTips result = new GrowingTips();

		NodeList childNodes = node.getChildNodes();

		for (int i = 0; i < childNodes.getLength(); i++) {
			Node n = childNodes.item(i);

			if (n.getNodeName().equals("tempreture")) {
				result.setTemperature(Integer.parseInt(n.getTextContent().strip()),
								n.getAttributes().getNamedItem("measure").getTextContent());

			} else if (n.getNodeName().equals("lighting")) {
				result.setLighting(n.getAttributes().getNamedItem("lightRequiring")
						.getTextContent().strip());

			} else if (n.getNodeName().equals("watering")) {
				result.setWatering(Integer.parseInt(n.getTextContent().strip()),
								n.getAttributes().getNamedItem("measure").getTextContent());
			}
		}
		return result;
	}

	private String getSimpleStringValue(Node node) {
		return node.getTextContent().strip();
	}

	public Flowers getFlowers() {
		if (Objects.isNull(xmlFileName))
			throw new IllegalArgumentException();

		LinkedHashMap<String, String> config = new LinkedHashMap<>();
		List<Flower> result = new ArrayList<>();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			// for security
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(xmlFileName);

			NamedNodeMap namedConfig = doc.getElementsByTagName("flowers").item(0).getAttributes();
			for (int i = 0; i < namedConfig.getLength(); i++) {
				config.put(namedConfig.item(i).getNodeName(), namedConfig.item(i).getTextContent());
			}

			// optional, but recommended
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("flower");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Flower flower = new Flower();
				NodeList list = nodeList.item(i).getChildNodes();

				for (int j = 0; j < list.getLength(); j++) {
					Node n = list.item(j);

					switch (n.getNodeName()) {
						case "name":
							flower.setName(getSimpleStringValue(n));
							break;
						case "soil":
							flower.setSoil(getSimpleStringValue(n));
							break;
						case "origin":
							flower.setOrigin(getSimpleStringValue(n));
							break;
						case "visualParameters":
							flower.setVisualParameters(getVisualParameter(n));
							break;
						case "growingTips":
							flower.setGrowingTips(getGrowingTips(n));
							break;
						case "multiplying":
							flower.setMultiplying(getSimpleStringValue(n));
							break;
					}
				}
				result.add(flower);
			}
		} catch (ParserConfigurationException | SAXException | IOException ex) {
			ex.printStackTrace();
		}

		return new Flowers(config, result);
	}


	public boolean writeInXML(Flowers flowers, String outputXML) throws ParserConfigurationException {

		LinkedHashMap<String, String> configuration = flowers.getConfiguration();

		List<Flower> flowersList = flowers.getFlowers();

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element root = doc.createElement("flowers");
		doc.appendChild(root);

		for (Map.Entry<String, String> entry: configuration.entrySet()) {
			root.setAttribute(entry.getKey(), entry.getValue());
		}

		for (Flower flower: flowersList) {
			String name = flower.getName();
			String soil = flower.getSoil();
			String origin = flower.getOrigin();
			VisualParameters visualParameters = flower.getVisualParameters();
			GrowingTips growingTips = flower.getGrowingTips();
			String multiplying = flower.getMultiplying();

			Element flowerElement = doc.createElement("flower");
			root.appendChild(flowerElement);

			Element nameEl = doc.createElement("name");
			nameEl.appendChild(doc.createTextNode(name));
			flowerElement.appendChild(nameEl);

			Element soilEl = doc.createElement("soil");
			soilEl.appendChild(doc.createTextNode(soil));
			flowerElement.appendChild(soilEl);

			Element originEl = doc.createElement("origin");
			originEl.appendChild(doc.createTextNode(origin));
			flowerElement.appendChild(originEl);


			/* visualParameters */

			String stemColour = visualParameters.getStemColour();
			String leafColour = visualParameters.getLeafColour();
			List<AveLenFlower> aveLenFlowers = visualParameters.getAveLenFlowers();

			Element visualParametersEl = doc.createElement("visualParameters");
			flowerElement.appendChild(visualParametersEl);

			Element stemColourEl = doc.createElement("stemColour");
			stemColourEl.appendChild(doc.createTextNode(stemColour));
			visualParametersEl.appendChild(stemColourEl);

			Element leafColourEl = doc.createElement("leafColour");
			leafColourEl.appendChild(doc.createTextNode(leafColour));
			visualParametersEl.appendChild(leafColourEl);

			for (AveLenFlower el: aveLenFlowers) {
				Element aveLenFlowerEl = doc.createElement("aveLenFlower");
				aveLenFlowerEl.appendChild(doc.createTextNode(
						String.valueOf(el.getValue())));

				aveLenFlowerEl.setAttribute("measure", el.getMeasure());

				visualParametersEl.appendChild(aveLenFlowerEl);
			}


			/* growingTips */

			Temperature temperature = growingTips.getTemperature();
			String lighting = growingTips.getLighting();
			Watering watering = growingTips.getWatering();

			Element growingTipsEl = doc.createElement("growingTips");
			flowerElement.appendChild(growingTipsEl);

			Element tempretureEl = doc.createElement("tempreture");
			tempretureEl.appendChild(doc.createTextNode(
					String.valueOf(temperature.getValue())));
			tempretureEl.setAttribute("measure", temperature.getMeasure());
			growingTipsEl.appendChild(tempretureEl);

			Element lightingEl = doc.createElement("lighting");
			lightingEl.setAttribute("lightRequiring", lighting);
			growingTipsEl.appendChild(lightingEl);

			Element wateringEl = doc.createElement("watering");
			wateringEl.appendChild(doc.createTextNode(
					String.valueOf(watering.getValue())));
			wateringEl.setAttribute("measure", watering.getMeasure());
			growingTipsEl.appendChild(wateringEl);


			Element multiplyingEl = doc.createElement("multiplying");
			multiplyingEl.appendChild(doc.createTextNode(multiplying));
			flowerElement.appendChild(multiplyingEl);
		}


		// write dom document to a file
		try (FileOutputStream output =
					 new FileOutputStream(outputXML)) {

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(output);

			transformer.transform(source, result);

		} catch (IOException | TransformerException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}


}
